import Vue from 'vue'
import Router from 'vue-router'
import Landing from '../components/Landing.vue'
import Home from '../components/Home'
import StoriesBehind from '../components/StoriesBehind'

Vue.use(Router);

export default new Router({
    routes: [
        {path: '/', name: 'LandingPage', component: Landing},
        {path: '/home', name:'HomePage', component: Home},
        {path: '/storiesbehind', name:'StoriesBehindPage', component: StoriesBehind}
    ]
})